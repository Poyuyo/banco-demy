package com.folcademy.bancodemy.models.entities;

import com.folcademy.bancodemy.models.enums.Currency;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity(name = "transactions")
public class TransactionsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Date date;
    private String description;
    private BigDecimal amount;
    @Enumerated(EnumType.STRING)
    private Currency currency;
    private String origin;
    private String destination;

    public TransactionsEntity() {
    }

    public TransactionsEntity(Date date, String description, BigDecimal amount, Currency currency, String origin, String destination) {
        this.date = date;
        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.origin = origin;
        this.destination = destination;
    }

    public Long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }
}

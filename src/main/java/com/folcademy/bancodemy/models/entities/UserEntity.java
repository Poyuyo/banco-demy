package com.folcademy.bancodemy.models.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "users")
public class UserEntity {
    @Id
    private String dni;
    private String firstName;
    private String lastName;
    private String address;
    private String username;
    private String password;

    public UserEntity() {
    }

    public UserEntity(String dni, String firstName, String lastName, String address, String password, String username) {
        this.dni = dni;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.password = password;
        this.username = username;
    }



    public String getDni() {
        return dni;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String fullName() {
        return firstName + " " +lastName;
    }
}

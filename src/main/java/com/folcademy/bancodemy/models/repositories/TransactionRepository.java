package com.folcademy.bancodemy.models.repositories;

import com.folcademy.bancodemy.models.entities.TransactionsEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransactionRepository extends CrudRepository<TransactionsEntity, Long> {
    TransactionsEntity findAllById(Long id);

    List<TransactionsEntity> findAllByOrigin(String numOrigin);

    boolean existsByOrigin(String numOrigin);

    List<TransactionsEntity> findAllByOriginOrDestination(String accountNumber, String accountNumber1);
}

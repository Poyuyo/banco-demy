package com.folcademy.bancodemy.models.repositories;

import com.folcademy.bancodemy.models.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<UserEntity, String> {

    Optional<UserEntity> findByDni (String userDni);

    Optional<UserEntity> findByUsername(String username);
}

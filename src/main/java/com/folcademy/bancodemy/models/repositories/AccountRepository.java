package com.folcademy.bancodemy.models.repositories;

import com.folcademy.bancodemy.models.entities.AccountEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends CrudRepository<AccountEntity, Long> {
    List<AccountEntity> findAllByUserId(String userId);

    Optional<AccountEntity> findByNumber(Long number);

    Optional<AccountEntity> findByCbu(String cbu);

}

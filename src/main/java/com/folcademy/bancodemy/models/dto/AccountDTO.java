package com.folcademy.bancodemy.models.dto;

public class AccountDTO {

    private Long number;
    private String cbu;
    private  String userId;

    public AccountDTO(Long number, String cbu, String userId) {
        this.number = number;
        this.cbu = cbu;
        this.userId = userId;
    }

    public AccountDTO() {
    }

    public Long getNumber() {
        return number;
    }

    public String getCbu() {
        return cbu;
    }

    public String getUserId() {
        return userId;
    }
}

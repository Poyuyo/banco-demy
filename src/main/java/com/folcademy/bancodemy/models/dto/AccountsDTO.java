package com.folcademy.bancodemy.models.dto;

import java.util.List;

public class AccountsDTO {

    List<AccountDTO> accountDTOList;

    public AccountsDTO() {
    }

    public AccountsDTO(List<AccountDTO> accountDTOList) {
        this.accountDTOList = accountDTOList;
    }

    public List<AccountDTO> getAccountDTOList() {
        return accountDTOList;
    }

    public int getAccountsNumber(){
        return accountDTOList.size();
    }
}

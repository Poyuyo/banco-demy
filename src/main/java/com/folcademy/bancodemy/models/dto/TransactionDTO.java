package com.folcademy.bancodemy.models.dto;

import java.math.BigDecimal;

public class TransactionDTO {
    private String date;
    private String description;
    private BigDecimal amount;
    private String currency;
    private String from;
    private String to;
    private String type;

    public TransactionDTO() {
    }

    public TransactionDTO(String date, String description, BigDecimal amount, String currency, String from, String to, String type) {
        this.date = date;
        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.from = from;
        this.to = to;
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getType() {
        return type;
    }
}

package com.folcademy.bancodemy.models.dto;

public class UserAccountDTO {
    private String firstName;
    private String lastName;
    private String cbu;

    public UserAccountDTO() {
    }

    public UserAccountDTO(String firstName, String lastName, String cbu) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cbu = cbu;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCbu() {
        return cbu;
    }
}

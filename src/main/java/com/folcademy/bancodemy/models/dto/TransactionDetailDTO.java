package com.folcademy.bancodemy.models.dto;

import java.math.BigDecimal;

public class TransactionDetailDTO {
    private String description;
    private BigDecimal amount;
    private String currency;
    private UserAccountDTO from;
    private UserAccountDTO to;

    public TransactionDetailDTO() {
    }

    public TransactionDetailDTO(String description, BigDecimal amount, String currency, UserAccountDTO from, UserAccountDTO to) {
        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.from = from;
        this.to = to;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public UserAccountDTO getFrom() {
        return from;
    }

    public UserAccountDTO getTo() {
        return to;
    }
}

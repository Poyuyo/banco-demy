package com.folcademy.bancodemy.models.dto;

import com.folcademy.bancodemy.models.enums.AccountType;

public class NewAccountDTO {

    private Long number;
    private AccountType type;
    private String userId;

    public NewAccountDTO() {
    }

    public NewAccountDTO(Long number, AccountType type, String userId) {
        this.number = number;
        this.type = type;
        this.userId = userId;
    }

    public Long getNumber() {
        return number;
    }

    public AccountType getType() {
        return type;
    }

    public String getUserId() {
        return userId;
    }
}

package com.folcademy.bancodemy.models.dto;

public class NewUserDTO {

    private String dni;
    private String firstName;
    private String lastName;
    private String address;
    private String username;
    private String password;

    public NewUserDTO() {
    }

    public NewUserDTO(String dni, String firstName, String lastName, String address, String username, String password) {
        this.dni = dni;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.username = username;
        this.password = password;
    }

    public String getDni() {
        return dni;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}

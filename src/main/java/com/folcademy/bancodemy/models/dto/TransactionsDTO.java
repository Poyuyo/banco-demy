package com.folcademy.bancodemy.models.dto;

import java.math.BigDecimal;
import java.util.List;

public class TransactionsDTO {

    List<TransactionDTO> transactions;
    private BigDecimal balance;

    public TransactionsDTO() {
    }

    public TransactionsDTO(List<TransactionDTO> transactions, BigDecimal balance) {
        this.transactions = transactions;
        this.balance = balance;
    }

    public List<TransactionDTO> getTransactions() {
        return transactions;
    }

    public int getTransaccionsNumber(){
        return transactions.size();
    }

    public BigDecimal getBalance() {
        return balance;
    }
}

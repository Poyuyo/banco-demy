package com.folcademy.bancodemy.models.dto;

import com.folcademy.bancodemy.models.enums.Currency;

import java.math.BigDecimal;

public class NewTransactionDTO {
    private String description;
    private BigDecimal amount;
    private Currency currency;
    private String from;
    private String to;

    public NewTransactionDTO() {
    }

    public NewTransactionDTO(String description, BigDecimal amount, Currency currency, String from, String to) {
        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.from = from;
        this.to = to;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }
}

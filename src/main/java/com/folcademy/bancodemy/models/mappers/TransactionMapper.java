package com.folcademy.bancodemy.models.mappers;

import com.folcademy.bancodemy.models.dto.NewTransactionDTO;
import com.folcademy.bancodemy.models.dto.TransactionDTO;
import com.folcademy.bancodemy.models.entities.TransactionsEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class TransactionMapper {
    public TransactionsEntity mapTransactionDtoToTransactionEntity(NewTransactionDTO newTransactionDTO){
        return new TransactionsEntity(new Date(),
                newTransactionDTO.getDescription(),
                newTransactionDTO.getAmount(),
                newTransactionDTO.getCurrency(),
                newTransactionDTO.getFrom(),
                newTransactionDTO.getTo());
    }

    public TransactionDTO mapTransactionEntityToTransactionDto(TransactionsEntity transactionsEntity, String type){
        return new TransactionDTO(transactionsEntity.getDate().toString(),
                transactionsEntity.getDescription(),
                transactionsEntity.getAmount(),
                transactionsEntity.getCurrency().toString(),
                transactionsEntity.getOrigin(),
                transactionsEntity.getDestination(),
                type);
    }
}

package com.folcademy.bancodemy.models.mappers;

import com.folcademy.bancodemy.models.dto.UserDTO;
import com.folcademy.bancodemy.models.entities.UserEntity;
import org.springframework.stereotype.Component;
import com.folcademy.bancodemy.models.dto.NewUserDTO;

@Component
public class UserMapper {

    public UserDTO mapUserEntityToUserDto(UserEntity userEntity) {
        return new UserDTO(userEntity.getDni(),
                userEntity.getFirstName(),
                userEntity.getLastName(),
                userEntity.getAddress());
    }

    public UserEntity mapUserDtoToUserEntity(UserDTO userDTO) {
        return new UserEntity(userDTO.getDni(),
                userDTO.getFirstName(),
                userDTO.getLastName(),
                userDTO.getAddress(), "password", "username");
    }

    public UserEntity mapUserDtoToNewUserEntity(NewUserDTO newUserDTO) {
        return new UserEntity(newUserDTO.getDni(),
                newUserDTO.getFirstName(),
                newUserDTO.getLastName(),
                newUserDTO.getAddress(),
                newUserDTO.getPassword(),
                newUserDTO.getUsername());
    }
}

package com.folcademy.bancodemy.models.mappers;

import com.folcademy.bancodemy.models.dto.AccountDTO;
import com.folcademy.bancodemy.models.dto.NewAccountDTO;
import com.folcademy.bancodemy.models.entities.AccountEntity;
import org.springframework.stereotype.Component;

@Component
public class AccountMapper {


    public AccountDTO mapAccountEntityToAccountDto(AccountEntity accountEntity) {
        return new AccountDTO(accountEntity.getNumber(), accountEntity.getCbu(), accountEntity.getUserId());
    }

    public AccountEntity mapAccountDtoToAccountEntity(NewAccountDTO newAccountDTO, String cbu) {
        return new AccountEntity(newAccountDTO.getNumber(),
                cbu,
                newAccountDTO.getType(),
                newAccountDTO.getUserId());
    }

}

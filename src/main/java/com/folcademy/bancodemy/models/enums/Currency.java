package com.folcademy.bancodemy.models.enums;

public enum Currency {
    ARS, USD, EUR
}

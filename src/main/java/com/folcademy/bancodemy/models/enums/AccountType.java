package com.folcademy.bancodemy.models.enums;

public enum AccountType {
    CUENTA_CORRIENTE, CAJA_DE_AHORROS
}

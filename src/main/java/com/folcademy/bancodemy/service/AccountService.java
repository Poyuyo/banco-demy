package com.folcademy.bancodemy.service;

import com.folcademy.bancodemy.exceptions.BadRequestException;
import com.folcademy.bancodemy.models.dto.AccountDTO;
import com.folcademy.bancodemy.models.dto.AccountsDTO;
import com.folcademy.bancodemy.models.dto.NewAccountDTO;
import com.folcademy.bancodemy.models.entities.AccountEntity;
import com.folcademy.bancodemy.models.mappers.AccountMapper;
import com.folcademy.bancodemy.models.repositories.AccountRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService {

    private final AccountRepository accountRepository;
    public final AccountMapper accountMapper;

    public AccountService(AccountRepository accountRepository, AccountMapper accountMapper) {
        this.accountRepository = accountRepository;
        this.accountMapper = accountMapper;
    }


    public AccountsDTO getAccounts(String userId) {
        List<AccountEntity> accountEntities = accountRepository.findAllByUserId(userId);
        List<AccountDTO> accountDTOList = new ArrayList<>();

        for (AccountEntity accountEntity : accountEntities) {
            accountDTOList.add(accountMapper.mapAccountEntityToAccountDto(accountEntity));
        }

        return new AccountsDTO(accountDTOList);

    }

    public ResponseEntity<String> createAccount(NewAccountDTO newAccountDTO) {
        Optional<AccountEntity> optionalAccountEntity = accountRepository.findByNumber(newAccountDTO.getNumber());
        if (optionalAccountEntity.isPresent()) {
            throw new BadRequestException("El numero de cuenta ya existe");
        }

        //Cbu  de 6 digitos
        Integer cbu = (int) (100000 + Math.random() * 900000);

        //Busco si ya existe cbu
        Boolean exist = accountRepository.findByCbu(cbu.toString()).isPresent();

        while (exist) {
            cbu = (int) (100000 + Math.random() * 900000);
            exist = accountRepository.findByCbu(cbu.toString()).isPresent();
        }

        AccountEntity accountEntity = accountMapper.mapAccountDtoToAccountEntity(newAccountDTO, cbu.toString());
        accountRepository.save(accountEntity);
        return new ResponseEntity<>("Ok", HttpStatus.OK);


    }
}

package com.folcademy.bancodemy.service;

import com.folcademy.bancodemy.exceptions.BadRequestException;
import com.folcademy.bancodemy.exceptions.NotFoundException;
import com.folcademy.bancodemy.models.dto.*;
import com.folcademy.bancodemy.models.entities.AccountEntity;
import com.folcademy.bancodemy.models.entities.TransactionsEntity;
import com.folcademy.bancodemy.models.entities.UserEntity;
import com.folcademy.bancodemy.models.mappers.TransactionMapper;
import com.folcademy.bancodemy.models.repositories.AccountRepository;
import com.folcademy.bancodemy.models.repositories.TransactionRepository;
import com.folcademy.bancodemy.models.repositories.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionsService {
    private final TransactionRepository transactionRepository;
    public final AccountRepository accountRepository;
    public final UserRepository userRepository;
    public final TransactionMapper transactionMapper;

    public TransactionsService(TransactionRepository transactionRepository, AccountRepository accountRepository, UserRepository userRepository, TransactionMapper transactionMapper) {
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
        this.transactionMapper = transactionMapper;
    }

    public TransactionsDTO getTransactions(Long accountNumber) {
        List<TransactionsEntity> transactionsEntities = transactionRepository.findAllByOriginOrDestination(accountNumber.toString(), accountNumber.toString());
        List<TransactionDTO> transactionDTOList = new ArrayList<>();

        for (TransactionsEntity transactionsEntity : transactionsEntities) {

            String transactionType;

            if (transactionsEntity.getOrigin().equals(accountNumber.toString())) {
                transactionType = "GASTO";
            } else {
                transactionType = "INGRESO";
            }

            transactionDTOList.add(transactionMapper.mapTransactionEntityToTransactionDto(transactionsEntity, transactionType));
        }

        BigDecimal balance = BigDecimal.ZERO;

        for (TransactionDTO transactionDTO : transactionDTOList) {

            if (transactionDTO.getType().equals("GASTO")) {
                balance = balance.subtract(transactionDTO.getAmount());
            } else {
                balance = balance.add(transactionDTO.getAmount());
            }
        }

        return new TransactionsDTO(transactionDTOList, balance);
    }

    public ResponseEntity<String> createTransaction(NewTransactionDTO newTransactionDTO) {

        if (newTransactionDTO.getFrom().equals(newTransactionDTO.getTo())) {
            throw new BadRequestException("Error al crear la transacción, from y to no pueden ser iguales");
        }
        if (!accountRepository.existsById(Long.parseLong(newTransactionDTO.getFrom()))) {
            throw new BadRequestException("Error al crear la transacción, la cuenta origen no existe");
        }
        if (!accountRepository.existsById(Long.parseLong(newTransactionDTO.getTo()))) {
            throw new BadRequestException("Error al crear la transacción, la cuenta destino no existe");
        }
        if (newTransactionDTO.getAmount().compareTo(new BigDecimal(0)) <= 0) {
            throw new BadRequestException("Error al crear la transacción, el monto no puede ser cero o negativo");
        }

        TransactionsEntity transactionsEntity = transactionMapper.mapTransactionDtoToTransactionEntity(newTransactionDTO);
        transactionRepository.save(transactionsEntity);
        return new ResponseEntity<>("Ok", HttpStatus.OK);

    }

    public TransactionDetailDTO getTransactionDetail(Long transactionNumber) {

        Optional<TransactionsEntity> transactionsEntityOptional = transactionRepository.findById(transactionNumber);

        if (transactionsEntityOptional.isPresent()) {

            TransactionsEntity transactionsEntity = transactionsEntityOptional.get();

            UserAccountDTO userAccountDTOFrom = getUserDTO(transactionsEntity.getOrigin());
            UserAccountDTO userAccountDTOTo = getUserDTO(transactionsEntity.getDestination());

            TransactionDetailDTO transactionDetailDTO = getDetailsDTO(transactionsEntity, userAccountDTOFrom, userAccountDTOTo);

            return transactionDetailDTO;
        } else {
            throw new NotFoundException("No existe la transacción");
        }
    }

    private UserAccountDTO getUserDTO(String account) {
        //Estas excepciones suceden debido a que tenia la bd cargada con valores sin validar de los desafios anteriores
        //cargando la bd con las restricciones nuevas no deberian suceder

        Optional<AccountEntity> optionalAccountEntity = accountRepository.findByNumber(Long.parseLong(account));
        if (optionalAccountEntity.isEmpty()) {
            throw new NotFoundException("No existe cuenta");
        }
        AccountEntity accountEntity = optionalAccountEntity.get();

        Optional<UserEntity> optionalUserEntity = userRepository.findByDni(accountEntity.getUserId());
        if (optionalUserEntity.isEmpty()) {
            throw new NotFoundException("No existe usuario registrado");
        }
        UserEntity userEntity = optionalUserEntity.get();

        return new UserAccountDTO(userEntity.getFirstName(),
                userEntity.getLastName(),
                accountEntity.getCbu());
    }


    private TransactionDetailDTO getDetailsDTO(TransactionsEntity transactionsEntity, UserAccountDTO userAccountDTOOrigin, UserAccountDTO userAccountDTODestination) {
        TransactionDetailDTO transactionDetailDTO = new TransactionDetailDTO(transactionsEntity.getDescription(),
                transactionsEntity.getAmount(),
                transactionsEntity.getCurrency().toString(),
                userAccountDTOOrigin,
                userAccountDTODestination);

        return transactionDetailDTO;
    }
}

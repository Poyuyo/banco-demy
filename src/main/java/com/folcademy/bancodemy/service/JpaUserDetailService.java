package com.folcademy.bancodemy.service;

import com.folcademy.bancodemy.models.entities.UserEntity;
import com.folcademy.bancodemy.models.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class JpaUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserEntity> optionalUserEntity = userRepository.findByUsername(username);

        if (optionalUserEntity.isEmpty()) {
            throw new UsernameNotFoundException("Not found " + username);
        }
        UserEntity userEntity = optionalUserEntity.get();
        return new User(userEntity.getUsername(), userEntity.getPassword(), new ArrayList<>());
    }
}

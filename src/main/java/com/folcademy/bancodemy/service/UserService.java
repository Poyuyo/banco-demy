package com.folcademy.bancodemy.service;

import com.folcademy.bancodemy.exceptions.BadRequestException;
import com.folcademy.bancodemy.exceptions.NotFoundException;
import com.folcademy.bancodemy.models.dto.UserDTO;
import com.folcademy.bancodemy.models.entities.UserEntity;
import com.folcademy.bancodemy.models.mappers.UserMapper;
import com.folcademy.bancodemy.models.repositories.UserRepository;
import com.folcademy.bancodemy.models.dto.NewUserDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;


    public UserService(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    public UserDTO getUser(String userDni) {
        Optional<UserEntity> optionalUserEntity = userRepository.findByDni(userDni);
        if (optionalUserEntity.isPresent()) {
            UserEntity userEntity = optionalUserEntity.get();
            UserDTO userDto = userMapper.mapUserEntityToUserDto(userEntity);
            return userDto;
        } else {
            throw new NotFoundException("No existe el usuario");
        }
    }

    public ResponseEntity<String> createUser(NewUserDTO newUserDTO) {
        Optional<UserEntity> optionalUserEntity = userRepository.findByDni(newUserDTO.getDni());
        if (optionalUserEntity.isEmpty()) {
            UserEntity userEntity = userMapper.mapUserDtoToNewUserEntity(newUserDTO);
            userRepository.save(userEntity);
            return new ResponseEntity<>("Ok", HttpStatus.OK);
        } else {
            throw new BadRequestException("Ya existe usuario con ese dni");
        }
    }
}

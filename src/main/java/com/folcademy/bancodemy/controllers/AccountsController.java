package com.folcademy.bancodemy.controllers;

import com.folcademy.bancodemy.models.dto.AccountsDTO;
import com.folcademy.bancodemy.models.dto.NewAccountDTO;
import com.folcademy.bancodemy.service.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountsController {

    private final AccountService accountService;

    public AccountsController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/{userId}")
    public ResponseEntity<AccountsDTO> getUserAccounts(@PathVariable String userId){
        return new ResponseEntity<>(accountService.getAccounts(userId), HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<String> newAccount(@RequestBody NewAccountDTO newAccountDTO){
        return accountService.createAccount(newAccountDTO);
    }
}

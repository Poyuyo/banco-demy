package com.folcademy.bancodemy.controllers;

import com.folcademy.bancodemy.exceptions.NotAuthorizedException;
import com.folcademy.bancodemy.models.dto.AuthenticateRequest;
import com.folcademy.bancodemy.models.dto.AuthenticationResponse;
import com.folcademy.bancodemy.security.JwtUtil;
import com.folcademy.bancodemy.service.JpaUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
public class AuthenticateController {

    @Autowired
    private JpaUserDetailService jpaUserDetailService;
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponse> createToken(@RequestBody AuthenticateRequest authenticateRequest) {
        UserDetails userDetails;
        try {
            userDetails = jpaUserDetailService.loadUserByUsername(authenticateRequest.getUsername());
        }catch (UsernameNotFoundException e){
            throw new NotAuthorizedException("El usuario o la contraseña son incorrectos");
        }

        /*if (Objects.isNull(userDetails) || !userDetails.getPassword().equals(authenticateRequest.getPassword())) {
            throw new NotAuthorizedException("El usuario o la contraseña son incorrectos");
        }*/

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authenticateRequest.getUsername(), authenticateRequest.getPassword())
        );

        String jwt = "Bearer "+ jwtUtil.generateToken(userDetails);
        AuthenticationResponse response = new AuthenticationResponse(jwt);
        return ResponseEntity.ok(response);
    }
}

package com.folcademy.bancodemy.controllers;

import com.folcademy.bancodemy.models.dto.NewTransactionDTO;
import com.folcademy.bancodemy.models.dto.TransactionDetailDTO;
import com.folcademy.bancodemy.models.dto.TransactionsDTO;
import com.folcademy.bancodemy.models.repositories.TransactionRepository;
import com.folcademy.bancodemy.service.TransactionsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

    private final TransactionsService transactionsService;

    public TransactionController(TransactionsService transactionsService) {
        this.transactionsService = transactionsService;
    }

    @GetMapping("/{transactionNumber}")
    public ResponseEntity<TransactionDetailDTO> getTransaction(@PathVariable Long transactionNumber) {
        return new ResponseEntity<>(transactionsService.getTransactionDetail(transactionNumber), HttpStatus.OK);
    }

    @GetMapping("/all/{accountNumber}")
    public ResponseEntity<TransactionsDTO> getTransactionsForAccount(@PathVariable Long accountNumber) {
        return new ResponseEntity<>(transactionsService.getTransactions(accountNumber), HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<String> createTransaction(@RequestBody NewTransactionDTO newTransactionDTO) {
        return transactionsService.createTransaction(newTransactionDTO);
    }

}

package com.folcademy.bancodemy.controllers;

import com.folcademy.bancodemy.models.dto.UserDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.folcademy.bancodemy.service.UserService;
import com.folcademy.bancodemy.models.dto.NewUserDTO;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{userDni}")
    public ResponseEntity<UserDTO> getUser(@PathVariable String userDni){
        return new ResponseEntity<>(userService.getUser(userDni), HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<String> newUser(@RequestBody NewUserDTO newUserDTO){
        return userService.createUser(newUserDTO);
    }
}

package com.folcademy.bancodemy.exceptions;

import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<ErrorMessage> defaultHandler(HttpServletRequest request, Exception e){
        e.printStackTrace();
        return new ResponseEntity<>(new ErrorMessage("Error genérico",
                e.getMessage(),
                "1",
                request.getRequestURI()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({BadRequestException.class, HttpMessageNotReadableException.class})
    @ResponseBody
    public ResponseEntity<ErrorMessage> badRequestrHandler(HttpServletRequest request, Exception e){
        return new ResponseEntity<>(new ErrorMessage("Bad Request",
                e.getMessage(),
                "2",
                request.getRequestURI()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseBody
    public ResponseEntity<ErrorMessage> notFoundrHandler(HttpServletRequest request, Exception e){
        return new ResponseEntity<>(new ErrorMessage("Not Found",
                e.getMessage(),
                "3 ",
                request.getRequestURI()),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({NotAuthorizedException.class, UsernameNotFoundException.class, BadCredentialsException.class})
    @ResponseBody
    public ResponseEntity<ErrorMessage> notAuthorizedHandler(HttpServletRequest request, Exception e){
        return new ResponseEntity<>(new ErrorMessage("Not Authorized",
                e.getMessage(),
                "4",
                request.getRequestURI()),
                HttpStatus.UNAUTHORIZED);
    }
}

package com.folcademy.bancodemy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoDemyApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoDemyApplication.class, args);
	}

}
